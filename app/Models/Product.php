<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'category',
        'description',
        'quantity',
        'date_availability',
        'price'
    ];

    public function images()
    {
        return $this->hasMany('App\Models\ProductImage');
    }
}
