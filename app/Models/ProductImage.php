<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'products_images';

    protected $fillable = [
        'name',
        'product_id',
    ];

    public function getNameAttribute($value)
    {
        return  'files/products_images/'.$value;
    }
}
