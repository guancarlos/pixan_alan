@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @if(Session::has('message'))
            <div class="flash-message">
                <p class="alert alert-success">{{ Session::get('message') }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            </div> <!-- end .flash-message -->
        @endif
        <div class="col-md-8 col-md-offset-2">
            <div class="h4">
                Mis productos
                <a class="pull-right" href="{{url('/add')}}">
                    Agregar
                </a>
            </div>
            <div class="row">
                @forelse ($products as $product)
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <img class="img-responsive" src="{{$product->images()->first()->name}}" />
                                <strong>{{$product->name}}</strong>
                                <p>
                                    {{$product->description}}
                                </p>
                                <strong class="pull-right h3">${{$product->price}}</strong>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p>Agrega Productos nuevos  dando click <a href="{{url('/add')}}">Aqui.</a></p>
                            </div>
                        </div>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection
