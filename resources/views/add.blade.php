@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">
@endsection


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="h4">
                <a href="{{url('/')}}">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    Regresar a mis productos
                </a>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body text-center" >
                        <h3>Registra un producto para vender</h3>
                        <p>
                            Escribe la informacion de tu producto, una fotografia y la fecha y hora
                            en que dejas de recibir pedidos para el producto.
                        </p>
                    </div>
                </div>
            </div>
            <form action="/add" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="text-center">Informacion general de tu producto</h3>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>El nombre de tu producto</label>
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="ej.Monitor LG de 23'" required/>
                                    @if ($errors->has('name'))
                                        <span class="help-block errors_input">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Categoria de tu producto</label>
                                    <input type="text" class="form-control" name="category" value="{{old('category')}}" placeholder="ej.Electronicos" required/>
                                    @if ($errors->has('category'))
                                        <span class="help-block errors_input">
                                            <strong>{{ $errors->first('category') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Descripcion del producto</label>
                                    <textarea class="form-control" name="description" placeholder="ej.Monitor Full HD LG,23 pulgadas,Resolucion 1920x1080px" required>{{old('description')}}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block errors_input">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>¿Cuantos productos quieres vender?</label>
                                    <input type="text" class="form-control" name="quantity" value="{{old('quantity')}}" placeholder="ej.1" required/>
                                    @if ($errors->has('quantity'))
                                        <span class="help-block errors_input">
                                            <strong>{{ $errors->first('quantity') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>¿Hasta cuando estara disponible?</label>
                                    <input type="text" class="form-control" name="date_availability" value="{{old('date_availability')}}" id="date_availability" placeholder="Selecciona una fecha y hora" required/>
                                    @if ($errors->has('date_availability'))
                                        <span class="help-block errors_input">
                                            <strong>{{ $errors->first('date_availability') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>¿Que precio tiene tu producto?</label>
                                    <input type="text" class="form-control" name="price" value="{{old('price')}}" placeholder="ej.3000 (usa solo numeros)" required/>
                                    @if ($errors->has('price'))
                                        <span class="help-block errors_input">
                                            <strong>{{ $errors->first('price') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body text-center">
                            <h3 class="text-center">Fotografia de tu producto</h3>
                            <p>
                                Presiona aquí para subir una o más fotografías, o arrastra las imágenes de tu producto,
                                puedes reordenar las imagenes. La fotogrfia principal de tu producto sera la que este
                                al lado izquierdo de este recuadro.
                                <div class="form-group">
                                    <input type="file" class="form-control" name="images[]" multiple accept="image/*"/>
                                    @if ($errors->has('images'))
                                        <span class="help-block errors_input">
                                            <strong>{{ $errors->first('price') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
                <input type="submit" class="btn btn-danger pull-right" value="PUBLICAR" />
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript"src="/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript"src="/js/moment.js"></script>
<script type="text/javascript"src="/js/transition.js"></script>
<script type="text/javascript"src="/js/collapse.js"></script>
<script type="text/javascript"src="/js/bootstrap.min.js"></script>
<script type="text/javascript"src="/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"src="/js/add.js"></script>
<script>

</script>
@endsection
